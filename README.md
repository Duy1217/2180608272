# 2180608272 / 21DTHD2

| **Function**                      | **Describe** |
|-----------------------------------|-----------------------------------------|
| **Value Statement**               | To manage the system effectively, admins can perform the functions of logging in, registering, editing information, deleting information and changing passwords.
| **Acceptance Criteria**           |<ins>**_Scenrio Criteria 1:_**</ins><br> GIVEN an admin with appropriate permissions.<br>WHEN the admin navigates to the login page and enters valid login credentials,<br>THEN the system should authenticate the admin and grant access to the admin dashboard.<br><ins>**_Scenrio Criteria 2:_**</ins><br> GIVEN an admin with appropriate permissions.<br>WHEN the admin accesses the registration page and fills out the required user details,<br>THEN the system should create a new account with the provided information.<br><ins>**_Scenrio Criteria 3:_**</ins><br> GIVEN an admin with appropriate permissions.<br>WHEN the admin selects the option to edit user information,<br>THEN the system should allow the admin to modify the necessary user details and save the changes.<br><ins>**_Scenrio Criteria 4:_**</ins><br>GIVEN an admin with appropriate permissions.<br>WHEN the admin selects the option to delete user information,<br>THEN the system should remove the user's information from the system and any associated data or records.<br><ins>**_Scenrio Criteria 5:_**</ins><br>GIVEN an admin with appropriate permissions.<br>WHEN the admin selects the option to change the password,<br>THEN the system will prompt the administrator to enter the current password and the new password for confirmation, and upon successful confirmation, the system will update the password.|
| **Definition of Done**            |  Unit Tests Passed.<br> Acceptance Criteria Met.<br> Code Reviewed.<br> Functional Tests Passed.<br> Non - Functional Requirements Met<br> Product Owner Accepcts User Story. |
| **Owner**                         | Nguyễn Phước Duy |
| **Iteration**                     | System management is expected to be implemented in part 1 of the library management application development project. |

![Screenshot_2023-10-17_112955](/uploads/67f18b90b553edb3e37e1e13f949c1a5/Screenshot_2023-10-17_112955.png)

|**Test case ID**|**Test objective**|**Precondition**|**Steps**|**Test data**|**Expected result**|**Post-condition**|**Status**|
|----------------|------------------|------------------|------------------|------------------|------------------|------------------|------------------|
|TC_001		 |Confirm that admin can log in, create accounts, edit information, delete information, and change passwords.|The administrator has a valid account and appropriate access rights.|1. Log in.<br><br>2. Register a new account.<br><br>3. Edit user information.<br><br>4. Delete the account.<br><br>5. Change the password|1. Enter valid login information.<br>Click the login button.<br><br>2. Enter all required information.<br>Click the register button.<br><br>3. Enter all required information.<br>Click the register button.<br><br>4. Select user information to delete.<br>Press the delete button.<br><br>5. Enter your current password and new password.<br>Select the change password button.|Log in to the form to operate, register a new account, edit user information, delete account and change password.|You can log in, register, edit information, delete information and change the updated password in the system.|**PASS**|








------------------------------------------------------------------------------------------------------------------------------------

# 2180607481 / 21DTHD2

| **Function**                      | **Describe** |
|-----------------------------------|-----------------------------------------|
| **Name**                          | Library management software - Create and print library cards to easily manage staff and readers.
| **Value Statement**               | As a library manager, i want create and print library cards. This cards can help us manage library staff, readers. In addition, the card helps us manage borrowing and returning books and paying for them.
| **Acceptance Criteria**           |  Create library-specific cards upon request from admin. <br>Check for errors when entering user information into the card. <br>Manage user information, allowing information to be edited if there are changes. <br>In addition, the cards be created help keep time for employees and manage payments for readers. <br>Compile necessary statistics and report user information as required by the admin (functions such as: readers who use cards the most, employees who work the most days,etc.) |
| **Definition of Done**            | Unit Tests Passed, Acceptance Criteria Met, Code Review, Functional Tests Passed, Product Owner Accepts User Story|
| **Owner**                         | Trần Thanh Hiền - 2180607481 |
| **Iteration**                     | The Creation and Printion library cards function is expected to be implemented in the 2th part of the library management application development project. |

![Screenshot_2023-10-11_153927](/uploads/ac37535121754112759dc698920595f3/Screenshot_2023-10-11_153927.png)


------------------------------------------------------------------------------------------------------------------------------------
# 2180607551
| Tiltle | library manager - look up the location of a book so I can find it quickly in the library.  |
| ---------- | ---------- |
| **Value Statement** | As a library patron,I want to be able to easily find the location of a bookso I can find it quickly in the library.  |
| **Acceptance criteria** | **Acceptance criteria 1**: When I access the library's catalog system, there will be a search bar displayed prominently on the home page.<p>**Acceptance criteria 2**: I can enter the title, author, or ISBN of the book I'm looking for into the search bar.<p>**Acceptance criteria 3** : After entering a search query and submitting it, I see a list of search results.<p>**Acceptance criteria 4** : Each search result displays the book's title, author, availability, and location in the library.<p>**Acceptance criteria 5** : If books are available, the location must specify the price or area where the books can be found.<p>**Acceptance criteria 6** : If the book is currently checked out, the availability status will indicate the expected return date.<p>**Acceptance criteria 7** : I will have the option to further refine my search by using filters such as book genre, year of publication, or language.<p>**Acceptance criteria 8** : When I select a specific book from search results, I can see additional details about that book, including a summary, cover art, and reviews if available.<p>**Acceptance criteria 9** : If the book is part of a series, series information will also be displayed.<p>**Acceptance criteria 10** : Search results and book details must be presented in an intuitive and user-friendly way, making it easy for me to find the desired book in the library.|
| **Definition of Done** |* The search functionality is implemented and integrated into the library's catalog system.<p>* The search bar allows users to enter the title, author, or ISBN of the book.<p>* The search query is processed, and relevant search results are retrieved from the library's database.<p>* The search results display the book's title, author, availability status, and location within the library.<p>* The book's location specifies the shelf or section where the book can be found within the library.<p>* Users have the option to further refine their search using filters such as genre, publication year, or language.<p>* Search results and book details are presented in a user-friendly and intuitive manner, ensuring ease of use for library patrons.<p>* Documentation, such as user guides or help documents, is updated to include instructions on how to use the book location lookup feature. |
|**owner**|Huy |
|**Interation**| |
|**Estimate**| 5Poitn|

![Git](/uploads/cb6930baa2b90e39e588070b8da7363c/Git.png)



------------------------------------------------------------------------------------------------------------------------------------
# 2180607480 / 21DTHD2

| **Title**             | Manage book warehouse|
|-------------------|----------------------|
| **Value Statement** | As a library manager, I want to manage libraries at school and company, so I can manage books more easily.|
|**Acceptance Criteria** |<p>**Acceptance Criteria 1:** I want to be able to add new books to the library's inventory so that I can keep track of all the books available.<p>**Acceptance Criteria 2:** I want to be able to update the information about a book in the inventory so that I can keep it accurate.<p>**Acceptance Criteria 3:** I want to be able to delete books from the inventory so that I can remove books that are no longer available.<p>**Acceptance Criteria 4:** I want to be able to search the inventory by title, author, or subject so that I can quickly find the books I need. <p>**Acceptance Criteria 5:** I want to be able to view a list of all the books in the inventory so that I can get an overview of the library's collection.|
|**Definition of Done** | <p> I can enter the title, author, subject, and number of copies of the new book.<p> The new book is added to the inventory.<p> I can view the new book in the inventory. |
| **Owner** | VAN HUY |
| **Ieration** |Unscheduled |
| **Estimate** | 3 Points |

![Screenshot_2023-10-12_152152](/uploads/1158782876cc2155a91dacc1f6923e62/Screenshot_2023-10-12_152152.png)




------------------------------------------------------------------------------------------------------------------------------------
# Ten: Truong Thanh Dat ##
MSSV: 218067432 

Lop : 21DTHD2 


| Title                  | Manager borrows and returns books  |
| ---------------------- | ---------------------------------- |
| **Value Statment**     | As a library manager, I want to manage readers' borrowing and returning books, so that I can inventory and manage books in more detail.                   |
| **Acceptance Criteria**|<ins>**_Acceptance Criteria 1_**</ins>:<br> + Given the reader has selected a book to borrow and that book is available in the library . <br> + When the library manager enters reader and book information into the system, including loan date and expiration date.<br> + Then the system provides information about expiration dates and rules for borrowing books to readers.<p> <br> <ins> **_Acceptance Criteria 2_**</ins>:<br> + Given is data about borrowing and returning books available in the system.<br> +  When the library manager creates reports and statistics about book borrowing and returning.<br> +  Then the system displays reports and statistics, including the number of books borrowed, the number of times the book is late, and the levels of book usage over time.                       |
|  **Defintion of Done**                    |  +   Check out book <br> +  Manage reader information <br> +  Report  <br> + Test Case Complete                                 |
|**Owner**|Dat|
|**Iteration**| Unscheduled|
|**Estimate**| 3 point|

![WDF](/uploads/c091a1ba6cf8e5ff6c593e2157930ee0/WDF.png)


